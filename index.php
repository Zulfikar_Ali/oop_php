<?php
    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');
    $mobil = new Animal("Shaun");
    echo "Name : " . $mobil -> name . "<br>";
    echo "Legs : " . $mobil -> legs . "<br>";
    echo "Cold Blooded : " . $mobil -> blood . "<br> <br>";

    $frog = new Frog("Buduk");
    echo "Name : " . $frog -> name . "<br>";
    echo "Legs : " . $frog -> legs . "<br>";
    echo "Cold Blooded : " . $frog -> blood . "<br>";
    echo "Jump : ";
    echo $frog -> Jump();
    echo "<br> <br>";

    $ape = new Ape("Kera Sakti");
    echo "Name : " . $ape -> name . "<br>";
    echo "Legs : " . $ape -> legs . "<br>";
    echo "Cold Blooded : " . $ape -> blood . "<br>";
    echo "Jump : ";  
    echo $ape -> Yell();
?>